import React from 'react'
import './css/main.css'
import Container from './layouts/Container';
import AppProvider from './helpers/contextApp';


function App() {
  return (
    <AppProvider>
      <Container />
    </AppProvider>
  );
}

export default App;

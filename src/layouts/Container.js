import React from 'react'

import Header from '../components/Header/header'
import Footer from '../components/Footer/footer'
import ContainerOrdenes from '../views/OrdenCompra/ContainerOrdenes'

const Container = () =>{
  return(
    <div className='container'>
      <Header />
      <ContainerOrdenes />
      <Footer />
    </div>
  )
}

export default Container
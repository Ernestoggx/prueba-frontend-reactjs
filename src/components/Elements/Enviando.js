import React from 'react'
import { Modal, ModalBody } from 'reactstrap';
import Lottie  from "lottie-react";

import Envio from '../../assets/lottie/envio.json'
import Success from '../../assets/lottie/success.json'

const Enviando = ( { pagar, success } ) =>{

  const style = {
    background: '#fffff00'
  };

  return(
    <Modal
      isOpen = {pagar}
      className='modal-content-envio'
    >
      <ModalBody >
        {
          !success ?
            <Lottie
              animationData={Envio}
              style={style}
            />
          :
          <Lottie
            animationData={Success}
            style={style}
          />
        }
      </ModalBody>
    </Modal>
  )
}

export default Enviando
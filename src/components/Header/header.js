import React from 'react'
import { Navbar, NavbarBrand, NavbarText } from 'reactstrap';

const Header = () =>(
<div>
  <Navbar
    dark
    expand="md"
    light
    className='app-header'
  >
    <NavbarBrand href="https://envia.com/">
      <img width="200" src="https://s3.us-east-2.amazonaws.com/enviapaqueteria/uploads/landing/images/countries/MEX/logo-dark.svg" alt="" />
    </NavbarBrand>
    <NavbarText>
      Ordenes de compra
    </NavbarText>
  </Navbar>
</div>
)

export default Header

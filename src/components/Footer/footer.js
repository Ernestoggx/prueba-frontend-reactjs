import React from 'react'

const Footer = ( ) =>(
  <footer className="app-footer">© 2022 Envia.com. Todos los derechos reservados.</footer>
)

export default Footer

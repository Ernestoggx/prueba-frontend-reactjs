import React, { useEffect, useState, useContext } from 'react'
import { Row, Col, UncontrolledAlert } from 'reactstrap';
import { getData, sumaSubtotal } from '../../helpers/helpers'

import OrdenesCompra from './components/OrdenesCompra/OrdenesCompra'
import DetalleOrden from './components/DetalleOrden/DetalleOrden'
import Formulario from './components/Formulario';
import Enviando from '../../components/Elements/Enviando'

import { AppContext } from '../../helpers/contextApp'

const OrdenCompra = () =>{
  const { setSubtotal } = useContext(AppContext);
  const [pagar, setPagar] = useState(false);
  const [success, setSuccess] = useState(false);
  const [ordenes, setOrdenes] = useState([]);
  const [detOrden, setDetOrden] = useState({});
  const [canvas, setCanvas] = useState(false);
  const [mensaje, setMensaje] = useState({
    status: false,
    label: '',
    message: ''
  });

  const getProductos = async()=>{
    const productos = await getData('orders');
    console.log(productos);
    if(productos.success){
      setOrdenes(productos.orders)
      return;
    }
    setMensaje({
      status: true,
      label: 'danger',
      message: 'Ha ocurrido algo, intente más tarde'
    });
  }

  const getDetalleProducto = async ( item ) =>{
    setDetOrden(item);
    const sumsubtotal = await sumaSubtotal(item.items, setSubtotal);
  }

  const eliminarProducto = async ( id ) =>{
    const nueva_lista = detOrden.items.filter(item=> item.id !== id);
    setDetOrden({
      ...detOrden,
          items: nueva_lista
    })
    const sumsubtotal = await sumaSubtotal(nueva_lista, setSubtotal);
  }

  const RealizarPago = () =>{
      setSuccess(false);
      setPagar(true);
      setTimeout(()=>{
        setSuccess(true);
      },1500);
      setTimeout(()=>{
        setPagar(false);
      },3000);
  }

  useEffect(()=>{
    getProductos();
  },[]);

  return(
    <Row className='mt-5'>
      <Enviando 
        pagar = { pagar }
        success = { success }
      />
      <Formulario 
        canvas = { canvas } 
        setCanvas = { setCanvas }
        detOrden = { detOrden }
        setDetOrden = { setDetOrden }
      />
      <Col md={{ size: 12 }}>
        {
          mensaje.status ?
            <UncontrolledAlert color={mensaje.label}>
              {mensaje.message}
            </UncontrolledAlert>
          : null
        }
      </Col>
      <OrdenesCompra 
        ordenes = {ordenes}
        getDetalleProducto = { getDetalleProducto } 
      />
      <DetalleOrden 
        detOrden = { detOrden }
        setCanvas = { setCanvas }
        eliminarProducto = { eliminarProducto }
        RealizarPago = { RealizarPago }
      />
    </Row>
  )
}

export default OrdenCompra
import React from 'react'
import { Row, Col, Button } from 'reactstrap';

const DetalleOrden = ( { detOrden, setCanvas } ) =>(
  <Row>
    <Col md={{ size: 8 }}>
      <h1>Orden: #<b className='text-title'>{ detOrden.number }</b></h1>
    </Col>
    <Col md={{ size: 4 }}>
      <Button className='mb-3' onClick={()=>setCanvas(true)}><span className="fa fa-plus-circle" aria-hidden="true"></span> Agregar producto</Button>
    </Col>
    <Col md={{ size: 6 }}>
        <p className='mb-0'>Cliente: { `${ detOrden.billingAddress.firstName } ${ detOrden.billingAddress.lastName }  ` }</p>
        <p>Dirección: { `${ detOrden.billingAddress.address1 } ${ detOrden.billingAddress.city }  ` }</p>
    </Col>
  </Row>
)

export default DetalleOrden
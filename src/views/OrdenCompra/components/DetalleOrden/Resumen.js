import React, { useContext } from 'react'
import { Row, Col } from 'reactstrap';

import { AppContext } from '../../../../helpers/contextApp'

const Resumen = ( { detOrden }) => {
  const { subtotal } = useContext(AppContext);
  return(
    <Col  md={{ size: 4, offset: 8 }} xs={{ size: 8, offset: 4 }} >
      <Row>
        <Col>
          <h4>Subtotal:</h4>
        </Col>
        <Col>
          <p>${ subtotal }</p>
        </Col>
      </Row>
      <Row>
        <Col>
          <h4>Descuento:</h4>
        </Col>
        <Col>
          <p>${ detOrden.totals.discount }</p>
        </Col>
      </Row>
      <Row>
        <Col>
          <h4>Total</h4>
        </Col>
        <Col>
          <p>${ (subtotal - parseFloat(detOrden.totals.discount)).toFixed(2) }</p>
        </Col>
      </Row>
    </Col>
  )
}

export default Resumen
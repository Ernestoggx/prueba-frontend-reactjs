import React, { Fragment } from 'react'
import { Col, Card, CardBody, Button } from 'reactstrap';

import TarjetaProducto from './TarjetaProducto'
import Encabezado from './Encabezado'
import Resumen from './Resumen'

const DetalleOrden = ( { detOrden, setCanvas, eliminarProducto, RealizarPago } ) =>(
  <Col md={{ size: 8 }}>
    <Card body className='shadow'>
      <CardBody>
        {
          detOrden.id ?
            <Fragment>
              <Encabezado 
                detOrden = { detOrden }
                setCanvas = { setCanvas }
              />
              {
                detOrden.items ?
                  detOrden.items.map(item => (
                    <TarjetaProducto producto = { item } key = { item.id } eliminarProducto = { eliminarProducto } />
                  ))
                :
                  null
              }
              <Resumen 
                detOrden = { detOrden }
              />
              <Col>
                <Button
                  color="primary"
                  className='btn-pagar col-md-12'
                  onClick={()=>RealizarPago()}
                >
                  Pagar
                </Button>
              </Col>
            </Fragment>
          :
          <h2>Seleccione un número de orden</h2>
        }
      </CardBody>
    </Card>
  </Col>
)

export default DetalleOrden
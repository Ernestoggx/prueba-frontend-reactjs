import React from 'react'
import { Row, Col, Card, CardBody, Button } from 'reactstrap';

const TarjetaProducto = ( { producto, eliminarProducto } ) =>(
  <Row className='mb-2'>
    <Col md={{ size: 12 }}>
      <Card color='light' >
        <CardBody>
          <Row>
            <Col md={{ size: 3 }} xs={{ size: 4 }}>
              <div className='img-default'></div>
            </Col>
            <Col md={{ size: 9 }} xs={{ size: 8 }}>
              <Row>
                <Col md={{ size: 5 }}>
                <h3>{ producto.name }</h3>
                <p>SKU: { producto.sku }</p>
              </Col>
              <Col md={{ size: 2 }}>
                <p className='mb-0'><small>Cant.</small></p>
                <h5>{ producto.quantity }</h5>
              </Col>
              <Col md={{ size: 3 }}>
                <p className='mb-0'><small>Precio</small></p>
                <h5>{ producto.price }</h5>
              </Col>
              <Col md={{ size: 1 }}>
                <Button onClick={()=>eliminarProducto(producto.id)} color="danger" ><span className="fa fa-trash" aria-hidden="true"></span></Button>
              </Col>
              </Row>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </Col>
  </Row>
)

export default TarjetaProducto
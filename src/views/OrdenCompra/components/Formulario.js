import React, { useState, useContext, useEffect } from 'react'
import { Offcanvas, OffcanvasHeader, OffcanvasBody, Input, FormGroup, Label, Form, Button, UncontrolledAlert } from 'reactstrap';
import { sumaSubtotal } from '../../../helpers/helpers'

import { AppContext } from '../../../helpers/contextApp'

const Formulario = ( { canvas, setCanvas, detOrden, setDetOrden } ) => {
  const { setSubtotal } = useContext(AppContext);
  const [mensaje, setMensaje] = useState({
    status: false,
    label: '',
    message: ''
  })
  const [validacion, setValidacion] = useState({
    name: 0,
    sku: 0,
    quantity: 0,
    price: 0,
  })
  const [producto, setProducto] = useState({
    name: '',
    sku: '',
    quantity: '',
    price: '',
  })
  const { name, sku, quantity, price} = producto;
  const actualizaState = e =>{
    setProducto({
      ...producto,
        [e.target.name] : e.target.value
    })
  } 

  const submitFormulario = async () =>{
    setValidacion(producto);
    if( name === '' || sku === '' || quantity === 0 || price === 0 || quantity === '' || price === ''){
      return;
    }
    setDetOrden({
      ...detOrden,
          items: [
            ...detOrden.items,
            producto
          ]
      })
      
    setProducto({
      name: '',
      sku: '',
      quantity: '',
      price: '',
    })

    setMensaje({
      status: true,
      label: 'success',
      message: 'El producto se ha agregado correctamente'
    })
  }

  const actualizaSubtotal = async () =>{
    const sumsubtotal = await sumaSubtotal(detOrden.items, setSubtotal);
  }

  useEffect(()=>{
    actualizaSubtotal();
  },[detOrden])

  return(
    <Offcanvas
      direction="end"
      isOpen={canvas}
    >
      <OffcanvasHeader toggle={function noRefCheck(){setCanvas(false)}}>
        <h3>Nuevo producto</h3>
      </OffcanvasHeader>
      <OffcanvasBody>
        {
          mensaje.status ?
            <UncontrolledAlert color={mensaje.label}>
              {mensaje.message}
            </UncontrolledAlert>
          : null
        }
        <Form>
          <FormGroup>
            <Label for="name">
              Nombre del producto (*)
            </Label>
            <Input
              id="name"
              name="name"
              type="text"
              value = { name }
              onChange = {actualizaState}
              className = { validacion.name === '' ? "input-requerido" : ''} 
            />
          </FormGroup>
          <FormGroup>
            <Label for="sku">
              SKU(*)
            </Label>
            <Input
              id="sku"
              name="sku"
              type="text"
              value = { sku }
              onChange = {actualizaState}
              className = { validacion.sku === '' ? "input-requerido" : ''} 
            />
          </FormGroup>
          <FormGroup>
            <Label for="quantity">
              Cantidad (*)
            </Label>
            <Input
              id="quantity"
              name="quantity"
              type="number"
              value = { quantity }
              onChange = {actualizaState}
              className = { validacion.quantity === '' ? "input-requerido" : ''} 
            />
          </FormGroup>
          <FormGroup>
            <Label for="price">
              Precio (*)
            </Label>
            <Input
              id="price"
              name="price"
              type="number"
              value = { price }
              onChange = {actualizaState}
              className = { validacion.price === '' ? "input-requerido" : ''} 
            />
          </FormGroup>

          <Button className='col-md-12' type='button' onClick={submitFormulario}>
            Agregar producto
          </Button>
        </Form>
      </OffcanvasBody>
    </Offcanvas>
  )
}

export default Formulario
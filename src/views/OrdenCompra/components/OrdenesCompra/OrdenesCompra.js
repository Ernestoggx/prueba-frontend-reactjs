import React, { Fragment } from 'react'
import { Col, Card, CardBody, Button, Spinner } from 'reactstrap';

const OrdenesCompra = ( { ordenes, getDetalleProducto }) =>(
  <Fragment>
    <Col md={{ size: 4 }}>
      <Card body className='shadow'>
        <CardBody>
          <h3 className='text-title'>ORDENES DE COMPRA</h3>
          {
            ordenes.length > 0 ?
              ordenes.map(item=>(
                <Button
                className='m-1'
                color="primary"
                outline
                onClick={()=>getDetalleProducto(item)}
                key = { item.id }
                >
                  { item.number }
                </Button>
              ))
            :
            <Spinner color = 'info' />
          }
        </CardBody>
      </Card>
    </Col>
  </Fragment>
)

export default OrdenesCompra
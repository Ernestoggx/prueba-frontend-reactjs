import React, {createContext, useState} from 'react'

export const AppContext = createContext();

const AppProvider = (props) =>{
  const[subtotal, setSubtotal] = useState(0);

  return(
    <AppContext.Provider
      value={{
        subtotal,
        setSubtotal
      }}
    >
      {props.children}
    </AppContext.Provider>
  )
}

export default AppProvider

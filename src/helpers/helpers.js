const baseRuta = 'https://eshop-deve.herokuapp.com/api/v2/'
const TOKEN = "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJwUGFINU55VXRxTUkzMDZtajdZVHdHV3JIZE81cWxmaCIsImlhdCI6MTYyMDY2Mjk4NjIwM30.lhfzSXW9_TC67SdDKyDbMOYiYsKuSk6bG6XDE1wz2OL4Tq0Og9NbLMhb0LUtmrgzfWiTrqAFfnPldd8QzWvgVQ";


export async function getData(ruta){
  try{
    const requestOptions = {
      method: 'GET',
       headers: { 
        'Authorization': "Bearer " + TOKEN,
        'Content-Type': 'application/json' 
       }
    };

    let response = await fetch(baseRuta+ruta,requestOptions);
    let data = await response.json();
    return data;

  }catch(err){
    return {status:'error', message:'Ha ocurrido algo:' +err};
  }
}


export function sumaSubtotal( productos, setSubtotal){
  console.log(productos)
  const sumaPrecios = productos.reduce((prev, next) => prev + parseFloat(next.price), 0);
  setSubtotal((sumaPrecios).toFixed(2));
}